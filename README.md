# Blueprint

```mermaid
graph
    subgraph Physical [Physical]
        Switch
        Compute
        Router
        MAASNode
    end

    Apps -->|Use| Services
    Apps -->|Deployed on| Microk8s
    Services -->|Deployed on| Microk8s
    Microk8s -->|Deployed on| Ubuntu[Ubuntu VMs]
    Microk8s -->|Uses Persistent Storage| PVECeph
    PVECeph[PVE Ceph Cluster] -->|Configured on| PVECluster[PVE Cluster]
    PVECluster -->|Configured on| Proxmox[Proxmox VE 8.2 on Debian 12 Bookworm]
    Ubuntu -->|Deployed on| PVECluster
    Proxmox -->|Installed on| Compute[Compute Nodes]
    PVECeph -->|Uses OSDs| Compute
    MAAS -.->|Commissions| Compute
    MAAS -->|Installs| Proxmox
    MAAS --> MAASNode[MAAS Node]
    Switch <--> Compute
    Router <--> Switch
    Router <--> MAASNode

    subgraph Applications [Applications]
        Apps[<a href="https://example.com">Apps</a>]
        Services
        Microk8s
    end

    subgraph Infra [Infrastructure]
        Ubuntu
        PVECluster
        PVECeph
        Proxmox
    end

    subgraph ValueStream [Value Stream]
        Applications
        Infra
        Physical
    end

    Pulumi1[Pulumi1 - Bootstrap MAAS Node] -.-> MAASNode
    Pulumi2[Pulumi2 - Provisions Router] -.-> Router
    Pulumi3[Pulumi3 - MAAS Comission] -.-> MAAS
    Pulumi4[Pulumi4 - MAAS OS Deployment] -.-> Proxmox
    Pulumi5[Pulumi5 - Create PVE Cluster and Expose Ports] -.-> PVECluster
    Pulumi5[Pulumi5 - Create PVE Cluster and Expose Ports] -.-> Router
    Pulumi6[Pulumi6 - Provisions PVE Ceph and Expose Ports] -.-> PVECeph
    Pulumi6[Pulumi6 - Provisions PVE Ceph and Expose Ports] -.-> Router
    Pulumi7[Pulumi7 - Provisions Ubuntu] -.-> Ubuntu
    Pulumi8[Pulumi8 - Provisions Microk8s] -.-> Microk8s
    Pulumi9[Pulumi9 - Provisions Services] -.-> Services
    Pulumi10[Pulumi10 - Provisions Apps] -.-> Apps

    subgraph OnLAN [On LAN]
        Pulumi2 --> Pulumi1
        MAAS
    end

    subgraph OnMAASNode [On MAAS Node]
        Pulumi6 --> Pulumi5
        Pulumi5 --> Pulumi4
        Pulumi4 --> Pulumi3
        Pulumi3 --> Pulumi2
    end

    subgraph OnRemote [On Remote Control Node]
        Pulumi7 --> Pulumi6
        Pulumi8 --> Pulumi7
        Pulumi9 --> Pulumi8
        Pulumi10 --> Pulumi9
    end

    classDef base fill:#ff9,stroke:#333,stroke-width:2px;
    classDef infra fill:#ccf,stroke:#333,stroke-width:1px;
    classDef apps fill:#cfc,stroke:#333,stroke-width:1px;
    classDef automation fill:#fcf,stroke:#f66,stroke-width:2px;
    classDef code fill:#ccf,stroke:#333,stroke-width:1px;

    class OnLAN,OnMAASNode,OnRemote,Code code;
    class Compute,MAASNode,Router,Switch base;
    class Proxmox,PVECluster infra;
    class PVECeph,Ubuntu,Microk8s,Apps,Services apps;
    class Pulumi,MAAS automation;

```